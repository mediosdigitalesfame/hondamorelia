<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Promociones</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
         <?php include('contenido/analytics.php'); ?>

			<div class="page-banner">         

				<div class="container">
					<h2><strong>Promociones </strong></h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
					<div class="row">

	


		      <!--PROMOCIONES  -->

      
    <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/HRV.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">
                             <h2>CIVIC</h2>
                             <p align="justify"> 
                                Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. Mensualidades desde $3,388 en la compra de Honda® Civic® en versión EX MT modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $3,387.61, con un CAT de 33.37% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 

                         <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">

                              <a href="contacto.php"> <img alt="Honda promociones " src="promos/HRV.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">

                             <h2>HR-V</h2>
                             <p align="justify"> 
                                Mensualidad desde $3,110. Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. Mensualidades desde $3,110 en la compra de Honda® HR-V® en versión UNIQ MT modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $3,109.66 con un CAT de 32.61% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. Tasa desde 11.99%. Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® HRV® modelo Epic CVT Y Touring CVT 2018, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 38.53% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br>       

                         <!-- PROMO 1 ================================================= -->

                         <span class="single-project-content">
                             <a href="contacto.php"> <img alt="Honda promociones " src="promos/CITY.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                   
                         <div class="col-md-12">
                             <h2>CITY</h2>
                             <p align="justify">

                                Vigencia del 01 al 30 de junio de 2018 o hasta agotar existencia lo que suceda primero. Mensualidades desde 2,633 en la compra de Honda® City® en versión LX MT modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $2,632.25, con un CAT de 46.01% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. 0% de comisión por apertura en la compra de Honda® City® versión LX MT, LX CVT y EX CVT modelo 2017 y 2018, con un pago desde 10% (diez) de enganche y plazos de 12 a 72, con un CAT de 40.27% sin IVA promedio informativo para modelo 2017 y CAT de 55.75% sin IVA promedio informativo para modelo 2018, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. 1er. Servicio Gratis. Aplica en la compra de Honda® City® versión LX MT, LX CVT y EX CVT modelo 2017 y 2018, adquiridos durante el periodo de vigencia y en el primer servicio del programa de mantenimiento especificado en la póliza de garantía 5,000 Km o 6 meses lo que ocurra primero (cambio de aceite de motor, lavado y aspirado, las revisiones de: bandas de propulsión, conductos y manguera de frenos, componentes de suspensión, componentes de dirección, conexiones y líneas de combustible, ejes de mando y flechas, conexiones y mangueras de enfriamiento, sistema de escape, acumulador y conexiones) o 10,000 kilómetros o 12 meses lo que ocurra primero (cambio de aceite y filtro de motor, lavado y aspirado, las revisiones de: bandas de propulsión, conductos y manguera de frenos, componentes de suspensión, componentes de dirección, conexiones y líneas de combustible, ejes de mando y flechas, conexiones y mangueras de enfriamiento, sistema de escape, acumulador y conexiones) . Válido únicamente en los concesionarios Autorizados Honda®. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div> <br><br>  


                          <!-- PROMO 1 ================================================= -->


                         <span class="single-project-content">
                             <a href="contacto.php"> <img alt="Honda promociones " src="promos/CRV.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                   
                         <div class="col-md-12">
                             <h2>CR-V</h2>
                             <p align="justify">

                                <strong>
                                    Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. Mensualidades desde $4,026 en la compra de Honda® CR-V® en versión EX modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $4,025.29, con un CAT de 26.72% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.


                             </p><br>
                         </div> <br><br> 

                         <!-- PROMO 1 ================================================= -->

                         <span class="single-project-content">
                             <a href="contacto.php"> <img alt="Honda promociones " src="promos/TYPE-R.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                   
                         <div class="col-md-12">
                             <h2>TYPE-R</h2>
                             <p align="justify">

                                <strong>
                                    Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Type-R® modelo 2017 y 2018, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 33.96% sin IVA promedio informativo para modelo 2018 y CAT de 33.32% sin IVA promedio informativo para modelo 2017 , que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.

                             </p><br>
                         </div> <br><br>    

                         <!-- PROMO 1 ================================================= -->

                         <span class="single-project-content">
                             <a href="contacto.php"> <img alt="Honda promociones " src="promos/PILOT.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                   
                         <div class="col-md-12">
                             <h2>PILOT</h2>
                             <p align="justify">

                                <strong>
                                    Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Pilot® en versión Touring modelo 2018, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 33.09% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.

                             </p><br>
                         </div> <br><br>   


                         <!-- PROMO 1 ================================================= -->
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones" src="promos/BRV.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">

                             <h2>BRV</h2>
                             <p align=tify"> 
                                Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. Mensualidades desde $3,335.15 en la compra de Honda® BR-V® en versión UNIQ modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $3,335.15 sin seguro incluido. CAT de 47.51 % sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> 

                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones" src="promos/ODYSSEY.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">

                             <h2>ODDYSEY</h2>
                             <p align="justify"> 
                                Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Odyssey® en versión EXL y Touring modelo 2019, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 26.38% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br>  

                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones" src="promos/ACCORD.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">

                             <h2>ACCORD</h2>
                             <p align="justify"> 
                                Mensualidad desde $4,954. Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia o lo que suceda primero. Mensualidades desde $4,954 pesos en la compra de Honda® Accord® en versión Sport Plus modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $4,953.67, con un CAT de 28.33% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. Tasa desde 11.99%. Vigencia del 01 al 30 de Junio de 2018 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Accord® en versiones Ex CVT, Sport Plus y Touring modelo 2018, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 36.75% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br>  
     </div>
     </div>
     </div>
     </div>
     </div>
 
         <?php include('contenido/footer.php'); ?>

 </body>
</html>