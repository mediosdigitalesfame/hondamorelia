<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Aviso de Privacidad</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
         <?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Aviso de Privacidad</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">

				<div class="welcome-box2">
					<div class="container">
						<p><h4><strong>I. IDENTIFICACIÓN DEL RESPONSABLE</strong></h4><br>
                        
En Fame Corregidora S.A. de C.V., en lo sucesivo Fame Monarca, con domicilio en Blvd. Adolfo Ruíz Cortines No. 4000 BIS Col. Jardines del Pedregal Del. Alvaro Obregón, México D.F. C.P. 01900, le comunicamos que la información personal de todos nuestros clientes y prospectos a clientes es tratada de forma estrictamente confidencial, por lo que podrán sentirse plenamente seguros que al adquirir nuestros bienes y/o servicios, hacemos un esfuerzo permanente para resguardarla y protegerla.<br><br>


<h4><strong>II. MEDIOS DE OBTENCIÓN Y DATOS PERSONALES QUE SE RECABAN</strong></h4><br>

Fame Monarca, recaba los siguientes datos personales que son necesarios y aplicables para dar cumplimiento a las finalidades establecidas en el presente Aviso de Privacidad:<br><br>

<li><strong>A.</strong>	Datos personales de identificación, laborales, patrimoniales, financieros, datos de terceros (Al proporcionar los datos personales de terceros, se entiende que cuenta con el consentimiento del titular de los datos personales).<br>

</li>

<li>Datos personales sensibles: Requerimos su consentimiento expreso y por escrito para el tratamiento de sus datos personales sensibles, financieros y/o patrimoniales.
</li><br>

<li><strong>B.</strong>	Datos personales que recabamos de manera directa a través de la página web, correo electrónico y vía telefónica.<br>
<li>Datos personales de identificación.</li>
</li><br>

<li><strong>C.</strong>	Datos personales que recabamos de manera indirecta a través de transferencias de terceros y fuentes de acceso públicos que están permitidas por la Ley.<br>
<li>Datos personales de identificación. Se le informa que las imágenes y sonidos captados a través de un sistema de circuito cerrado, serán utilizados para su seguridad y de las personas que nos visitan.</li><br><br><br>


<h4><strong>III. FINALIDADES</strong></h4><br>

En Fame Monarca tenemos la filosofía de mantener una relación estrecha y activa con nuestros clientes y prospectos a clientes. En términos de lo establecido por la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y su Reglamento, los datos que nos proporcione serán utilizados para las siguientes finalidades:<br><br>

<li><strong>A.</strong>	Necesaria para la relación Jurídica con el responsable:<br>
Realizar los trámites, procedimientos y gestiones para la adquisición de bienes, productos y/o servicios para nuestros clientes y prospectos a clientes.<br>
Para la adquisición de automóviles y camiones.<br>
Para la comercialización de bienes, productos y/o servicios.<br>
Investigación para confirmar la información y documentación requerida y otorgada por el cliente.<br>
Trámite a nombre y por cuenta del cliente, con empresas de seguros y fianzas para adquirir una póliza de automóvil o camión, póliza de desempleo y cualquiera otra requerida en la relación de compra-venta.<br>
Consultar el historial crediticio ante las sociedades de información crediticia (Buró de Crédito).<br>
Investigación ante las dependencias oficiales correspondientes de las garantías otorgadas (Aval y Obligados Solidarios).<br>
Procesar solicitudes de facturación y aclaraciones.<br>
Gestiones de cobranza, incluyendo la búsqueda ante las autoridades de datos patrimoniales.<br>
Gestoría ante las autoridades competentes para el alta de los vehículos automotores ante el REPUVE.<br>
Dar cumplimiento a requerimientos legales.<br>
Mantener actualizados nuestros registros para poder responder a sus consultas e invitarle a eventos que tengan relación al bien adquirido por el cliente.<br>
La realización de cualquier actividad necesaria para el cumplimiento de nuestra relación contractual.
</li><br>

<li><strong>B.</strong>	No necesaria para la relación jurídica con el responsable:<br>
1.	Realizar Actividades de mercadotecnia, publicidad y prospección comercial, diversas al servicio que brinda Fame Monarca.<br>
2.	Realizar Actividades de mercadotecnia, publicidad y prospección comercial relacionada con los productos y/o servicios de Fame Monarca.<br><br>

Las finalidades no necesarias son importantes para ofrecerle a través de campañas de mercadotecnia, publicidad y prospección comercial, productos y servicios exclusivos, por lo que usted tiene derecho a oponerse, o bien, a revocar su consentimiento para que Fame Monarca deje de tratar sus datos personales para dichas finalidades.<br><br>

<p align="center"><font color="#000000"><strong>Autorización de los términos y condiciones del Aviso de Privacidad: ____ Acepto; ____No acepto</strong></font></p><br></li>
<br>
<br>
<br>

<h4><strong>IV. OPCIONES PARA LIMITAR EL USO O DIVULGACIÓN DE SUS DATOS PERSONALES.</strong></h4><br>

Fame Monarca le comunica que si usted desea dejar de recibir mensajes de mercadotecnia, publicidad o de prospección comercial, relativos a las finalidades referidas al aparto III inciso (b), puede hacerlo valer por medio del correo electrónico avisodeprivacidad@grupofame.com, así mismo, por este medio atenderemos todas sus dudas y comentarios acerca del tratamiento de sus datos personales.<br><br><br><br>


<h4><strong>V. SOLICITUD DE ACCESO, RECTIFICACIÓN, CANCELACIÓN U OPOSICIÓN DE DATOS PERSONALES (DERECHOS ARCO) Y REVOCACIÓN DE CONSENTIMIENTO.</strong></h4><br>

Todos sus datos personales son tratados de acuerdo a la legislación aplicable y vigente en el país, por ello le informamos que usted tiene en todo momento el derecho de Acceder, Rectificar, Cancelar, u Oponerse al tratamiento que le damos a sus datos personales, así como Revocar el consentimiento otorgado para el tratamiento de los mismos; derechos que podrá hacer valer a través de los Gerente y/o Jefes Administrativos, personas que Fame Monarca ha designado para tal efecto, o bien, enviando un correo electrónico a avisodeprivacidad@grupofame.com, o si lo prefiere, accediendo a nuestro sitio WEB www.famemonarca.com sección Aviso de Privacidad o al teléfono (443) 315 2244, para que le sea proporcionado el Formato de Solicitud de Derechos ARCO, mismo que deberá presentar requisitado de manera personal en el domicilio del Responsable, debiendo adjuntar una copia de su identificación oficial para acreditar su titularidad. La respuesta a su solicitud de Derechos ARCO se le hará llegar al correo electrónico que haya proporcionado, dentro del término de  20 días hábiles contados a partir de la recepción de dicha solicitud. Así mismo, se le informa que el derecho de acceso se tendrá por cumplido cuando se haga llegar la respuesta correspondiente a través del correo electrónico que usted nos haya indicado para tal efecto.<br><br>

En caso de no estar de acuerdo en el tratamiento de sus datos personales, puede acudir ante el IFAI.<br><br><br><br>

<h4><strong>VI. TRANSFERENCIAS DE DATOS PERSONALES.</strong></h4><br>

<li>Transferencia sin necesidad de consentimiento:<br>
Se le informa que a fin de dar cumplimiento a las finalidades establecidas en el apartado III inciso (a) del presente Aviso de Privacidad, sus datos personales pueden ser transferidos y tratados dentro y fuera de los Estados Unidos Mexicanos por personas distintas a Fame Monarca. En este sentido y con fundamento en La Ley y su Reglamento, sus datos personales podrán ser transferidos sin necesidad de su consentimiento.<br><br>
</li>

<li>Transferencias con consentimiento:<br>
Fame Monarca podrá transferir sus datos personales de identificación y de contacto a empresas de marketing y publicidad para las finalidades descritas en el apartado III del inciso (b) del presente Aviso de Privacidad.</li><br><br>
<br>

<h4><strong>VII. USO DE COOKIES</strong></h4><br>

Fame Monarca no utiliza “web beacons” para obtener información personal de usted de manera automática, pero puede utilizar "cookies" y conexiones de hipertexto o ligas (links) a través del sitio para mejorar y personalizar la visita del Usuario. El Usuario puede optar por desactivar la funcionalidad de las "cookies" dentro de su buscador (browser), pero al hacerlo el Usuario probablemente no podrá gozar de aspectos específicos de alguna parte de nuestros sitios. Los datos personales que recabamos de manera electrónica, así como las finalidades del tratamiento se encuentran establecidos en el presente Aviso de Privacidad.<br><br><br><br>


<h4><strong>VIII. MODIFICACIÓN AL AVISO DE PRIVACIDAD.</strong></h4><br>

Este Aviso de Privacidad podrá ser modificado de tiempo en tiempo por Fame Monarca, dicha modificación podrá consultarse a través de los siguientes medios:<br><br>

<li>
Nuestra página de internet www.hondamorelia.com (Sección Aviso de Privacidad).
</li>
<li>
Avisos visibles en nuestras instalaciones de Fame Monarca.</li>
<li>
Cualquier otro medio de comunicación que Fame Monarca, determine para tal efecto.
</li>
<p><strong>Estoy enterado del tratamiento que recibirán mis datos personales en términos de lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares.</strong></p><br>

<p align="center">
Fecha de elaboración del presente aviso 06/07/2011<br>
Fecha de última actualización 05/04/2017
</p>



						</p>
					</div>
				</div>

				<!-- services-box -->
				<div class="services-box">
					<div class="container">
						<div class="row">

							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
									<a class="services-icon2" href="pdfs/arco.pdf" target="_blank"><i class="fa fa-download"></i></a>
									<div class="services-post-content">
										<h4>Descarga</h4>
										<p>Haz clic en el ícono para descargar nuestro archivo de <strong>Derechos ARCO</strong></p>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
								</div>
							</div>

						</div>
					</div>
					<img class="shadow-image" alt="" src="images/shadow.png">
				</div>

			</div>
<?php include('contenido/footer.php'); ?>

</body>
</html>