<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							 
							<span><i class="fa fa-phone"></i>Agencia: (442) 210 2404 </span>
<!--                            <span><i class="fa fa-user"></i><a href="aviso.html">Aviso de privacidad</a></span>-->
						</p>
						<ul class="social-icons">
							<li><a class="facebook" href="https://www.facebook.com/grupofame/" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/HondaFame" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a class="whatsapp" href="http://www.grupofame.com/whatsapp/" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                            <li><a class="instagram" href="https://www.instagram.com/grupofame/" target="_blank"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.php" target="_self"><img alt="Inicio" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a class="active" href="index.php">Inicio</a></li>
                                                             
							<li class="drop"><a href="autos.php">Autos</a>
								<ul class="drop-down">
 
                                	<li><a href="pdfs/brv2018.pdf" target="_blank">BR-V</a></li>
                                	<li><a href="pdfs/fit2018.pdf" target="_blank">Fit</a></li>
                                	<li><a href="pdfs/city2018.pdf" target="_blank">City</a></li>
                                	<li><a href="pdfs/civic2018.pdf" target="_blank">Civic Sedán</a></li> 
                                    <li><a href="pdfs/civiccoupe2018.pdf" target="_blank">Civic Coupé</a></li> 
                                	<li><a href="pdfs/accord2018.pdf" target="_blank">Accord</a></li>
                                	<li><a href="pdfs/crv2018.pdf" target="_blank">CR-V</a></li>
                                    <li><a href="pdfs/hrv2018.pdf" target="_blank">HR-V</a></li>
                                    <li><a href="pdfs/pilot2018.pdf" target="_blank">Pilot</a></li>
                                    <li><a href="pdfs/odyssey2018.pdf" target="_blank">Odyssey</a></li>
                                    <li><a href="pdfs/typer.pdf" target="_blank">Type R</a></li>			

                                </ul> </li>  
                                                       
							<li><a href="promociones.php" target="_self">Promociones</a></li>                            
							<li><a href="http://www.fameseminuevos.com/" target="_blank">Seminuevos</a></li>
                            
                            <li><a href="blog.php" target="_self">Blog</a></li>
                            
                            <li><a href="ubicacion.php" target="_self">Ubicaciones</a></li>

                            <li><a href="contacto.php" target="_self">Contacto</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->
        
 

