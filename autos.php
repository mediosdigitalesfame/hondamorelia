<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Autos</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>

	 <div id="content">
		 <div class="page-banner">
			 <div class="container">
				 <h2>Vehículos</h2>
			 </div>
		 </div>

		 <div class="portfolio-box with-3-col">
			 <div class="container">
				 <ul class="filter">
					 <li><a href="#" class="active" data-filter="*"><i class="fa fa-th"></i>Mostrar todos</a></li>
					 <li><a href="#" data-filter=".auto">Autos</a></li>
					 <li><a href="#" data-filter=".suv">SUV's - Minivan</a></li>
					 <!--<li><a href="#" data-filter=".camioneta">Camioneta</a></li>
					 <li><a href="#" data-filter=".hibrido">Híbridos</a></li>--> 
				 </ul>

				 <div class="portfolio-container">

						 <div class="work-post auto"> 
							 <div class="work-post-gal">
								 <a href="pdfs/brv2018.pdf" target="_blank"><img alt="BR-V" src="images/autos/original/brv2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> BR-V <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/brv2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <a href="pdfs/typer.pdf" target="_blank"> <img alt="FIT" src="images/autos/original/typer.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> HONDA<sup>&reg;</sup> <font color="#DD1707">TYPE R</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/typer.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <a href="pdfs/accord2018.pdf" target="_blank"> <img alt="FIT" src="images/autos/original/accord2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> ACCORD<sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/accord2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					      <div class="work-post auto">
							 <div class="work-post-gal">
								 <a href="pdfs/civic2018.pdf" target="_blank"><img alt="FIT" src="images/autos/original/civic2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CIVIC <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/civic2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <span><a href="pdfs/civiccoupe2018.pdf" target="_blank"><img alt="FIT" src="images/autos/original/civiccoupe2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CIVIC COUPE <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/civiccoupe2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <a href="pdfs/city2018.pdf" target="_blank"><img alt="FIT" src="images/autos/original/city2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CITY <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/city2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <a href="pdfs/fit2018.pdf" target="_blank"><img alt="FIT" src="images/autos/original/fit2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> FIT <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/fit2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post suv">
							 <div class="work-post-gal">
								 <a href="pdfs/hrv2018.pdf" target="_blank"><img alt="FIT" src="images/autos/original/hrv2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> HR-V <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/hrv2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post suv">
							 <div class="work-post-gal">
								 <a href="pdfs/crv2018.pdf" target="_blank"><img alt="FIT" src="images/autos/original/crv2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CR-V<sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/crv2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post suv">
							 <div class="work-post-gal">
								 <a href="pdfs/pilot2018.pdf" target="_blank"><img alt="FIT" src="images/autos/original/pilot2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> PILOT<sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/pilot2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post suv">
							 <div class="work-post-gal">
								 <a href="pdfs/odyssey2018.pdf" target="_blank"><img alt="FIT" src="images/autos/original/odyssey2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> ODYSSEY <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/odyssey2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>


                        
						</div>
                    </div>
			</div>

		</div>
		<!-- FIN VEHICULOS -->
 <?php include('contenido/footer.php'); ?>

 </body>
</html>