<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<?php include('contenido/head.php'); ?>
</head>
    <body>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
		 <?php include('contenido/analytics.php'); ?>
		 <?php include('contenido/bienvenida.php'); ?>
			<div class="portfolio-box with-3-col">
				<div class="container">
                      <div class="portfolio-container">
							<div class="col-md-4">
                                        <div class="work-post">
                                            <div class="work-post-gal">
                                                <a href="http://hondaaltozano.com/contacto.php" target="_blank"><img alt="" src="images/altozano.png"></a>
                                            </div>
                                            <div class="work-post-content">
                                                <h5><a href="http://hondaaltozano.com/contacto.php" target="_blank"><strong>Honda® Altozano</strong></a></h5>

                                            </div>
                                        </div> 
							</div>

							<div class="col-md-4">
                                        <div class="work-post">
                                            <div class="work-post-gal">
                                                <a href="http://hondamanantiales.com/contacto.php" target="_blank"> <img alt="" src="images/manantiales.png"></a>
                                            </div>
                                            <div class="work-post-content">
                                                <h5><a href="http://hondamanantiales.com/contacto.php" target="_blank"><strong>Honda® Manantiales</strong></a></h5>
                                            </div>
                                        </div> 
							</div>
                            
							<div class="col-md-4">
                                        <div class="work-post">
                                            <div class="work-post-gal">
                                                <a href="http://famemonarca.com/contacto.php" target="_blank"><img alt="" src="images/monarca.png"></a>
                                            </div>
                                            <div class="work-post-content">
                                                <h5><a href="http://famemonarca.com/contacto.php" target="_blank"><strong>Honda® Monarca</strong></a></h5>
                                            </div>
                                        </div> 
							</div>                           

									</div>

						</div>
					</div>
               <?php include('contenido/vehiculos.php'); ?>
		<?php include('contenido/citas.php'); ?>

	
     </div >

     <?php include('contenido/footer.php'); ?>

</body>
</html>